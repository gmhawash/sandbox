enum = Enumerator.new do |y|
  y.yield 1
  y.yield 2
end

p enum.collect {|x| x*x }

class Handler
  def initialize(message, scope)
    @scope = scope
    @message = message
  end

  def process!
    @scope.instance_eval do
      p 'heelo'
      @retries -= 1
    end
  end
end

@retries = 3
begin
  p @retries
  raise 'hello'
rescue Exception => e
  p self
  Handler.new(e, self).process!
end
