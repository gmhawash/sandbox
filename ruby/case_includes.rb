class CaseInclude
  def initialize(includee)
    @includee = includee
  end
  def ===(collection)
    collection.include?(@includee)
  end
end

def Includes?(includee)
  CaseInclude.new(includee)
end

def includes?(element)
  lambda {|array| array.include?(element) }
end

def does_it_include?(array)
  puts case array
  when includes?('one') then 'one'
  when includes?(:one) then :symbol_one
  end
end


does_it_include? ['one', 'two', 'three']
does_it_include? [:one, 'two', 'three']
