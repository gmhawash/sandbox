def one
  yield
end

def two
  yield
end

one do
  begin
    p 'one'
    two do
      p 'two'
      begin
        p 'three'
        raise 'hello'
      rescue
        retry
      end
    end
  end
end
