#!/usr/bin/env ruby

def match_message(regexp)
  lambda{ |error| p error.class; regexp === error.message }
end

begin
  raise StandardError, "Error message about a socket."
rescue match_message(/socket/) => error
  puts "Error #{error} matches /socket/; ignored."
end

