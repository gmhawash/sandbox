require "google_drive"

# Logs in.
# You can also use OAuth. See document of
# GoogleDrive.login_with_oauth for details.
session = GoogleDrive.login("downhome.rails@gmail.com", "d0wnh0me")

sp = session.create_spreadsheet("My new sheet")
ws = sp.worksheets[0]
ws.title = "loans"
ws[2,1] = 'one'
ws.save



exit

# First worksheet of
# https://docs.google.com/spreadsheet/ccc?key=pz7XtlQC-PYx-jrVMJErTcg
worksheets = session.spreadsheet_by_key("0Aip3iiJjXgdCdFFCOVVwV3N6elVjcndhS1BVM284T0E").worksheets
p worksheets.count
p worksheets.map(&:title)

exit

# Gets content of A2 cell.
p ws[2, 1]  #==> "hoge"

# Changes content of cells.
# Changes are not sent to the server until you call ws.save().
ws[2, 1] = "foo"
ws[2, 2] = "bar"
ws.save()

# Dumps all cells.
for row in 1..ws.num_rows
  for col in 1..ws.num_cols
    p ws[row, col]
  end
end

# Yet another way to do so.
p ws.rows  #==> [["fuga", ""], ["foo", "bar]]

# Reloads the worksheet to get changes by other clients.
ws.reload()
