class Whatever < StandardError; end
begin
  raise Whatever
rescue Whatever => e
  p e.class
  case e
    when Whatever then p 'yes'
  end
p  Whatever === e
p  e.class === Whatever
rescue Exception => e
p  e === Whatever
end
