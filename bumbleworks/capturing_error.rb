require 'bundler'

Bundler.require

Bumbleworks.reset!
Bumbleworks.storage = {}
Bumbleworks.start_worker!

Bumbleworks.define_process 'cause_an_error' do
 mary_joe :task => 'hello'
 bobby
end

Bumbleworks.dashboard.register_participant 'mary_joe' do |workitem|
  raise 'I am an error'
end

p Bumbleworks.error_handlers

Bumbleworks.launch!('cause_an_error')

sleep 5
p Bumbleworks.logger.entries
p Bumbleworks.dashboard.ps
