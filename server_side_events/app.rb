# Usage: redis-cli publish message hello

require 'bundler'
Bundler.require

conns = []

get '/' do
  erb :index
end

get '/subscribe' do
  content_type 'text/event-stream'
  stream(:keep_open) do |out|
    conns << out
    out.callback { conns.delete(out) }
  end
end

Thread.new do
  100.times do
    conns.each do |out|
      out << "event: refresh\n\n"
      out << "data: hello\n\n"
    end
  end
end

__END__

@@ index
  Hello
  <article id="log"></article>

  <script>
    var source = new EventSource('/subscribe');

    source.addEventListener('refresh', function (event) {
      log.innerText += '\n' + event.data;
    }, false);
  </script>
